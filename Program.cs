﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;

namespace CloudSideApp
{
    class Program
    {
        // IoT Hub connection-string
        static string connectionString = "HostName=STM-test-iot.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=xUMWT2n5Ghw5BXYjXI2S3xidPnymrpQZtWqMbwVRIws=";

        static void Main(string[] args)
        {
            


            // Log
            Console.WriteLine("Starting device/module retriever application...");
            // Read input
            Console.WriteLine("Device kind:");
            Console.WriteLine("\t1. Connected device");
            Console.WriteLine("\t2. Module device");
            Console.WriteLine("Type choose: ");
            string choose = Console.ReadLine();

            try{
                int parsedChoose = Int32.Parse(choose);
                string deviceId;
                string edgeId;
                Twin twin;

                var registryManager = RegistryManager.CreateFromConnectionString(connectionString);
                var serviceClient = ServiceClient.CreateFromConnectionString(connectionString);

                switch(parsedChoose){
                    case 1: 
                        // Read additional input
                        Console.WriteLine("Device id:");
                        deviceId = Console.ReadLine();

                        registryManager.GetDeviceAsync(deviceId).Wait();
                        Device device = registryManager.GetDeviceAsync(deviceId).Result;
                        Console.WriteLine("Device '"+deviceId+"' is an IoT Edge: " +device.Capabilities.IotEdge); 
                        if(device.Capabilities.IotEdge){
                            registryManager.GetModulesOnDeviceAsync(deviceId).Wait();
                            var modules = registryManager.GetModulesOnDeviceAsync(deviceId).Result;
                            foreach(var module in modules){
                                Console.WriteLine("\t"+module.Id);
                            }
                        }
                        
                        break;
                    case 2: 
                        // Read additional input
                        Console.WriteLine("Edge device id:");
                        edgeId = Console.ReadLine();
                        Console.WriteLine("Device id:");
                        deviceId = Console.ReadLine();

                        // // Get edge device
                        // registryManager.GetModuleAsync(edgeId,deviceId).Wait();
                        // var module = registryManager.GetModuleAsync(edgeId,deviceId).Result;
                        
                        // Get related twin
                        registryManager.GetTwinAsync(edgeId,deviceId).Wait();
                        twin =registryManager.GetTwinAsync(edgeId,deviceId).Result;
                        Console.WriteLine(twin.ToJson());
                        break;
                    default: 
                        Console.WriteLine("Wrong input.");
                        break;
                }

            }catch(Exception e){
                Console.WriteLine("Something gone wrong.");
            }

            Console.WriteLine("Exiting...");

        }
    }
}
